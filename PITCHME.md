---?color=linear-gradient(90deg, white 50%, black 50%)

@snap[north-west span-15 ]
![simplon](assets/img/simplon.png)
@snapend
<br>
<br>
<br>
@snap[west span-50 ]

### Projet chef d'oeuvre
#### Plateforme de partage de compétences et de prestations
<br>

##### Par: Amal CHELBI
@snapend

@snap[east span-40]
![WELCOME](assets/img/cherry-welcome.png)
@snapend
---

@snap[west span-40 ]
![PLan](assets/img/mirage-list-is-empty.png)
@snapend

@snap[north span-70]
## PLan
@snapend
@snap[east span-70]
@ul[](false)
- Identification du besoin
- Description
- Technologie utilisée
- Etapes et démarche
- Fonctionnalités techniques
- Bibliographie de recherche
- Conclusion
@ulend
@snapend
---

@title[Contexte]
@snap[north  span-70 ]
### Identification du besoin
@snapend
@snap[east span-70]

@ul[](false)
#### - Manque de ressources et competances
-
#### - Difficulté de trouver des opportunités intéressante
@ulend
@snapend

@snap[west span-40]
![WELCOME](assets/img/cherry-payment-processed.png)
@snapend

---

@title[Description]

@snap[north silver-fox]
## Description
@snapend

#### - Une **plateforme** de partage et d'échange de prestations et de compétances

#### - Elle servira à mettre en relation les porteurs de projets avec les jeunes étudiants ou experts qui veulent aider

@snap[south-east span-20]
![Success](assets/img/cherry-success.png)
@snapend
---

@snap[west span-50]
## Technologie utilisée
@snapend

@snap[east span-40]
![WELCOME](assets/img/marginalia-programming.png)
@snapend

+++?image=assets/img/laravel-vue.png&size=40%
@snap[west span-30]

@ul[](false)
### Laravel
@ulend
@snapend

@snap[east span-30]

@ul[](false)
### Vue.js
@ulend
@snapend
---
@snap[west span-50]
![Etapes](assets/img/kingdom-uploading.png)
@snapend
@snap[east span-50]
### Etapes et démarche
@snapend

+++
@snap[west span-50]
### Avis des clients potentiels
@snapend

@snap[east span-50]
![Avis](assets/img/mirage-no-comments.png)
@snapend

+++

@snap[east span-40 ]
![Maquette](assets/img/maquette1.jpg)
@snapend
@snap[west span-40  ]
![Maquette](assets/img/maquette2.jpg)
@snapend
@snap[north span-40  ]
![Maquette](assets/img/maquette3.jpg)
@snapend

@snap[south span-40]
### Maquette
@snapend
+++

## Diagramme de cas d'utilisation

+++?image=assets/img/diagramme.png&size=45%
@title[Diagramme]
+++

@snap[north span-100]
![Maquette](assets/img/classe.jpg)
@snapend

@snap[south span-100]
### Diagramme de classe
@snapend
---
@snap[west span-40 text-center]
### Fonctionnalités techniques
@snapend

@snap[east span-40]
![recherche](assets/img/cherry-delivery.png)
@snapend
+++?image=assets/img/messengerCapture.png&size=90%

@snap[south span-100 text-center]
### Messagerie interne 
@snapend
+++?image=assets/img/filtreCapture.png&size=90%
@title[Filtre]
@snap[south span-100 text-center]
@box[bg-white box-padding](Filtre par région et par catégorie)
@snapend
---
@title[Demo]
![Video](https://www.youtube.com/embed/5oJ1pBJA6j8)
---
@snap[east span-40 text-center]
## Bibliographie de recherche
@snapend

@snap[west span-40]
![recherche](assets/img/cherry-searching.png)
@snapend

+++
@title[recherche]
@snap[north-east span-50]
![recherche](assets/img/stackOv.png)
@snapend

@snap[north-west span-50]
![recherche](assets/img/youtubeSearch.png)
@snapend

@snap[south-west span-50]
![recherche](assets/img/laravelCapture.png)
@snapend

@snap[south-east span-50]
![recherche](assets/img/github.png)
@snapend
---
@snap[west span-40 text-center]
## Problèmes et difficultés 
@snapend

@snap[east span-40]
![recherche](assets/img/cherry-fatal-error.png)
@snapend
+++?color=linear-gradient(100deg, white 50%, black 50%)

@snap[east span-40 pb]
Affichage des données filtrées
![recherche](assets/img/dataFiltre.png)
@snapend

@snap[west span-40]
## Affichage des images 
#### (Relations polymorphes)
![recherche](assets/img/imagesProj.png)
@snapend
---
@snap[north span-50 ]
## Challenge à relever
@snapend
@snap[east span-70]

@ul[](false)
- Corriger les erreurs
- Design
- Ajouter Commentaires
@ulend
@snapend
@snap[west span-40]
![challenge](assets/img/cherry-upgrade.png)
@snapend 
---?color=linear-gradient(90deg, white 50%, black 50%)

@snap[west span-40 text-center]
## Merci pour votre attention
@snapend

@snap[east span-40]
![Merci](assets/img/cherry-coming-soon.png)
@snapend

---